# Authentication Service
Distributed authentication system with Java services and Hazelcast as session storage cluster.

# Getting started

### Requirements
- Java 17+
- Docker & Docker Compose
- Make

### Running

```bash
make redeploy
```

This spins up the services defined in `docker-compose.yaml`:
- 2 instances of the authentication service
- 1 instance of HAproxy
- 1 instance of MariaDB
- Overlay network `mocknet` where the services are joined

If this is your first time running the system, then after the services have been started, run
```bash
make createdb
```

This initializes the database with seed data (`/files/database/seed.sql`). You might have to do `make redeploy` again 
because the services fail to connect to the database that didn't exist yet.

# Evaluation

Local environment is not optimal for performance testing because your system needs to both run the services
and also bomb them with requests. I ran the tests using Jmeter so that it sent 5000 requests with a ramp-up period of
5 seconds, meaning 1000 requests every second. Going above this would result in problems that would no longer 
reflect how the services would actually perform, because the test machine itself became the bottleneck.

I tested the system with one to three instances of the authentication service. For each configuration, I let Jmeter 
run for 3-4 minutes. The test plan is available in `files/performance/auth.jmx`. Every loop does 2 requests to start 
and then complete the authentication. The results are titled "combined" to signify that **the graph shows the combined 
average latency of the two requests**. Results for individual steps are also available in the `files/performance` folder.
As expected, the second step takes longer since it does more work.

![1000 requests per second](files/performance/results/1-service/5000r-5s.png)*1 service - 1000 requests per second*

![1000 requests per second](files/performance/results/2-services/5000r-5s.png)*2 services - 1000 requests per second*

![1000 requests per second](files/performance/results/3-services/5000r-5s.png)*3 services - 1000 requests per second*

From these results we can see that the benefits of multiple services did not become apparent with the tests I was able to perform. One service
alone performed better than two or three services, although three faired slightly better than two. Of course going
from one to two services means that Hazelcast's cluster has to actually synchronize the data which results in a slight
performance hit. This is also where the load balancer has to start distributing the requests.

At first, I forgot to set an eviction policy for Hazelcast, meaning the sessions would never be removed. This always 
resulted in the latency skyrocketing after the first minute. At this point, the store would contain around 1000 * 60 = 60 000 sessions.

To potentially see the benefits of this kind of a distributed system, we would need to deploy the services to the cloud
and run more intensive tests against that. Another way would be to use a PC powerful enough to run more requests as well as each 
service with entry-level cloud server specs like 2 CPUs and 2GB of RAM.

Another improvement I could do to the test plan would be to include a header in the response from HAproxy to also graph 
the response latency from each service individually.

I did also try Hazelcast's near cache pattern with the same two and three service configurations, but did not see any 
significant differences.

![1000 requests per second](files/performance/results/2-services/5000r-5s-nearcache.png)*2 services with near cache - 1000 requests per second*

![1000 requests per second](files/performance/results/3-services/5000r-5s-nearcache.png)*3 services with near cache- 1000 requests per second*