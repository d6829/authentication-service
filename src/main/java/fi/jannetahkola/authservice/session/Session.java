package fi.jannetahkola.authservice.session;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
public class Session implements Serializable {
    @NotNull
    private String id;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private SessionStatus status;
    @NotNull
    private String challenge;
    @NotNull
    @Builder.Default
    private Integer attempts = 0;

    public boolean isPending() {
        return this.status.equals(SessionStatus.PENDING);
    }

    public boolean isFailed() {
        return this.status.equals(SessionStatus.FAILED);
    }

    public boolean isCompleted() {
        return this.status.equals(SessionStatus.COMPLETED);
    }

    public Session incrementAttemptsOrFail() {
        this.attempts = attempts + 1;
        if (attempts >= 3)
            return this.toFailed();
        return this;
    }

    public Session toFailed() {
        SessionStatus target = SessionStatus.FAILED;
        if (this.status == SessionStatus.COMPLETED) {
            throw new RuntimeException(String.format(
                    "Invalid status change from %s to %s", this.status, target));
        }
        this.status = target;
        return this;
    }

    public Session toCompleted() {
        SessionStatus target = SessionStatus.COMPLETED;
        if (this.status == SessionStatus.FAILED) {
            throw new RuntimeException(String.format(
                    "Invalid status change from %s to %s", this.status, target));
        }
        this.status = target;
        return this;
    }
}
