package fi.jannetahkola.authservice.session;

import com.hazelcast.core.HazelcastInstance;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public record SessionService(HazelcastInstance hazelcastInstance) {
    private static final String MAP = "sessions";

    public Mono<Session> load(String sessionId) {
        return Mono.fromCallable(() ->
                        hazelcastInstance
                                .getMap(MAP)
                                .get(sessionId))
                .cast(Session.class);
    }

    public Mono<Session> store(Session session) {
        return Mono.fromRunnable(() ->
                hazelcastInstance.getMap(MAP)
                        .set(session.getId(), session))
                .thenReturn(session);
    }
}
