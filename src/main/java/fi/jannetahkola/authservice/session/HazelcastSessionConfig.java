package fi.jannetahkola.authservice.session;

import com.hazelcast.config.*;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import fi.jannetahkola.authservice.config.AppConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class HazelcastSessionConfig {
    private final AppConfig appConfig;

    @Bean
    Config sessionConfig() {
        Config config = new Config()
                .setClusterName("sessions")
                .setInstanceName("sessions")
                .setAdvancedNetworkConfig(
                        new AdvancedNetworkConfig()
                                .setEnabled(true)
                                .setJoin(
                                        new JoinConfig()
                                                .setTcpIpConfig(
                                                        new TcpIpConfig()
                                                                .setEnabled(true)
                                                                .setMembers(appConfig.getHazelcastMembers()))
                                                .setMulticastConfig(
                                                        new MulticastConfig()
                                                                .setEnabled(false))
                                                .setAutoDetectionConfig(
                                                        new AutoDetectionConfig()
                                                                .setEnabled(false)))
                                .setClientEndpointConfig(
                                        new ServerSocketEndpointConfig()
                                                .setPort(9090))
                                .setMemberEndpointConfig(
                                        new ServerSocketEndpointConfig()
                                                .setPort(5701)
                                                .setPortAutoIncrement(true)
                                                .setReuseAddress(true)
                                                .setSocketTcpNoDelay(true)));
        config.getMapConfig("sessions")
                .setTimeToLiveSeconds(60)
                .setMaxIdleSeconds(0)
                .setEvictionConfig(
                        new EvictionConfig()
                                .setEvictionPolicy(EvictionPolicy.LRU)
                                .setMaxSizePolicy(MaxSizePolicy.PER_NODE)
                                .setSize(10000));
        if (appConfig.getHazelcastNearCache()) {
            log.info("Using near cache");
            NearCacheConfig nearCacheConfig = new NearCacheConfig()
                    .setInMemoryFormat(InMemoryFormat.OBJECT)
                    .setInvalidateOnChange(false)
                    .setTimeToLiveSeconds(600)
                    .setEvictionConfig(
                            new EvictionConfig()
                                    .setEvictionPolicy(EvictionPolicy.NONE)
                                    .setMaxSizePolicy(MaxSizePolicy.ENTRY_COUNT)
                                    .setSize(10000));
            config.getMapConfig("sessions")
                    .setNearCacheConfig(nearCacheConfig);
        } else {
            log.info("NOT using near cache");
        }
        return config;
    }

    @Bean
    HazelcastInstance hazelcast(Config config) {
        return Hazelcast.getOrCreateHazelcastInstance(config);
    }
}
