package fi.jannetahkola.authservice.session;

public enum SessionStatus {
    PENDING, FAILED, COMPLETED
}
