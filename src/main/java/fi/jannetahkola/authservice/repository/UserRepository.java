package fi.jannetahkola.authservice.repository;

import fi.jannetahkola.authservice.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {}
