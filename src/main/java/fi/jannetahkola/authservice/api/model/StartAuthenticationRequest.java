package fi.jannetahkola.authservice.api.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Builder
public class StartAuthenticationRequest {
    @NotBlank
    @Pattern(regexp = "[\\d]{5}")
    private String username;

    @NotBlank
    private String password;
}
