package fi.jannetahkola.authservice.api.model;

import fi.jannetahkola.authservice.session.SessionStatus;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class CompleteAuthenticationResponse {
    @NotNull
    private SessionStatus status;
    private String token;
}
