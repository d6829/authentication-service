package fi.jannetahkola.authservice.api.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StartAuthenticationResponse {
    private String sessionId;
    private String challenge;
}
