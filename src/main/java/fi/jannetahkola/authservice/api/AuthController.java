package fi.jannetahkola.authservice.api;

import fi.jannetahkola.authservice.api.model.CompleteAuthenticationRequest;
import fi.jannetahkola.authservice.api.model.CompleteAuthenticationResponse;
import fi.jannetahkola.authservice.api.model.StartAuthenticationRequest;
import fi.jannetahkola.authservice.api.model.StartAuthenticationResponse;
import fi.jannetahkola.authservice.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping(
        value = "/auth",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class AuthController {
    private static final String SESSION_ID_PATH = "session-id";

    private final AuthService authService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<StartAuthenticationResponse> start(
            @Valid @RequestBody StartAuthenticationRequest startAuthenticationRequest) {
        return authService.start(startAuthenticationRequest);
    }

    @PatchMapping(value = "{" + SESSION_ID_PATH + "}")
    public Mono<CompleteAuthenticationResponse> complete(
            @PathVariable(SESSION_ID_PATH) String sessionId,
            @Valid @RequestBody CompleteAuthenticationRequest completeAuthenticationRequest) {
        return authService.complete(sessionId, completeAuthenticationRequest);
    }
}
