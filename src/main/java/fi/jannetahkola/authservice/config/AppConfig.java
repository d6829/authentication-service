package fi.jannetahkola.authservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Configuration
@ConfigurationProperties(value = "app")
@Data
@Validated
public class AppConfig {
    @NotNull
    private Boolean debug;

    @NotNull
    @Size(min = 1)
    private List<String> hazelcastMembers;

    @NotNull
    private Boolean hazelcastNearCache = false;
}
