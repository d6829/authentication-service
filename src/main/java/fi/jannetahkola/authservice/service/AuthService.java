package fi.jannetahkola.authservice.service;

import fi.jannetahkola.authservice.api.model.CompleteAuthenticationRequest;
import fi.jannetahkola.authservice.api.model.CompleteAuthenticationResponse;
import fi.jannetahkola.authservice.api.model.StartAuthenticationRequest;
import fi.jannetahkola.authservice.api.model.StartAuthenticationResponse;
import fi.jannetahkola.authservice.config.AppConfig;
import fi.jannetahkola.authservice.model.User;
import fi.jannetahkola.authservice.repository.UserRepository;
import fi.jannetahkola.authservice.session.Session;
import fi.jannetahkola.authservice.session.SessionService;
import fi.jannetahkola.authservice.session.SessionStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Objects;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthService {
    private final SessionService sessionService;
    private final ChallengeResponseService challengeResponseService;
    private final UserRepository userRepository;
    private final AppConfig appConfig;

    public Mono<StartAuthenticationResponse> start(StartAuthenticationRequest request) {
        return challengeResponseService.calculateChallenge(request.getUsername())
                .doOnSuccess(challengeResponse -> {
                    if (appConfig.getDebug()) {
                        log.info("challenge = {}, response = {}", challengeResponse.getA(), challengeResponse.getB());
                    } else {
                        log.info("challenge = {}", challengeResponse.getA());
                    }
                })
                .flatMap(challengeResponse -> sessionService.store(Session.builder()
                        .id(UUID.randomUUID().toString())
                        .username(request.getUsername())
                        .password(request.getPassword())
                        .status(SessionStatus.PENDING)
                        .challenge(challengeResponse.getA())
                        .build()))
                .doOnSuccess(session -> log.info("Started new authentication session {}", session))
                .map(session -> StartAuthenticationResponse.builder()
                        .sessionId(session.getId())
                        .challenge(session.getChallenge())
                        .build());
    }

    public Mono<CompleteAuthenticationResponse> complete(String sessionId, CompleteAuthenticationRequest request) {
        return sessionService.load(sessionId)
                .switchIfEmpty(Mono.error(
                        new ResponseStatusException(
                                HttpStatus.NOT_FOUND, "Session not found")))
                .flatMap(session -> {
                    if (!session.isPending()) {
                        log.warn("Attempted to complete an invalid session {}", session);
                        return Mono.error(
                                new ResponseStatusException(
                                        HttpStatus.BAD_REQUEST, "Invalid session"));
                    }
                    return Mono.just(session);
                })
                .flatMap(session ->
                        challengeResponseService
                                .verifyResponse(
                                        session.getChallenge(),
                                        request.getChallengeResponse(),
                                        session.getUsername())
                                .flatMap(success -> tryFetchUserAndCompleteSession(success, session)))
                .flatMap(sessionService::store)
                .flatMap(session -> {
                    if (session.isCompleted()) {
                        log.info("Complete authentication session {}", session);
                        return Mono.just(CompleteAuthenticationResponse.builder()
                                .status(session.getStatus())
                                .token("token")
                                .build());
                    }
                    if (session.isFailed())
                        return Mono.just(CompleteAuthenticationResponse.builder()
                                .status(session.getStatus())
                                .token("")
                                .build());
                    return Mono.error(
                            new ResponseStatusException(
                                    HttpStatus.BAD_REQUEST, "Invalid response challenge response"));
                });
    }

    private Mono<Session> tryFetchUserAndCompleteSession(Boolean success, Session session) {
        final String userId = session.getUsername();
        return Mono.fromCallable(() -> userRepository.findById(userId).orElse(new User()))
                .subscribeOn(Schedulers.boundedElastic())
                .map(user -> {
                    boolean credentialsValid =
                            Objects.equals(user.getId(), userId)
                                    && Objects.equals(user.getPassword(), session.getPassword());
                    if (!credentialsValid)
                        log.warn("Attempted to complete authentication " +
                                "with invalid credentials. Session: {}", session);
                    return success && credentialsValid
                            ? session.toCompleted()
                            : session.incrementAttemptsOrFail();
                });
    }
}
