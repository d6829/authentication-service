package fi.jannetahkola.authservice.service;

import fi.jannetahkola.authservice.model.Pair;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor
public class ChallengeResponseService {
    private final ChallengeCodeService challengeCodeService;

    public Mono<Pair<String, String>> calculateChallenge(String username) {
        return Mono.fromCallable(() -> {
            String challenge = challengeCodeService.getChallenge();
            String response = String.valueOf(Math.abs((username + challenge).hashCode() % 10000));
            return new Pair<>(challenge, response);
        });
    }

    public Mono<Boolean> verifyResponse(String challenge, String response, String username) {
        return Mono.fromCallable(() -> String.valueOf(
                Math.abs((username + challenge).hashCode() % 10000)).equals(response));
    }
}
