package fi.jannetahkola.authservice.service;

import fi.jannetahkola.authservice.config.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

@Service
public class ChallengeCodeService {
    private static final Integer MAX_LEN = 10000;
    private static final SecureRandom random = new SecureRandom();
    private final List<String> codes = new ArrayList<>();

    @Autowired
    public ChallengeCodeService(AppConfig appConfig) {
        if (appConfig.getDebug()) {
            codes.add("3729");
        } else {
            codes.addAll(
                    IntStream.rangeClosed(1, 10)
                            .mapToObj(ignored ->
                                    String.format("%04d", random.nextInt(MAX_LEN)))
                            .toList());
        }
    }

    public ChallengeCodeService(List<String> codes) {
        this.codes.addAll(codes);
    }

    public String getChallenge() {
        return codes.get(new Random().nextInt(codes.size()));
    }
}
