package fi.jannetahkola.authservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.jannetahkola.authservice.api.model.CompleteAuthenticationRequest;
import fi.jannetahkola.authservice.api.model.StartAuthenticationRequest;
import fi.jannetahkola.authservice.model.Pair;
import fi.jannetahkola.authservice.service.ChallengeCodeService;
import fi.jannetahkola.authservice.service.ChallengeResponseService;
import fi.jannetahkola.authservice.session.SessionStatus;
import fi.jannetahkola.authservice.util.AuthSpecifications;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"test", "local"})
class AuthServiceApplicationTests {
    static final String USERNAME = "123456789";
    static final String PASSWORD = "password";

    @Autowired
    private ObjectMapper jackson;
    @MockBean
    private ChallengeResponseService challengeResponseService;

    @BeforeEach
    void setup(@LocalServerPort Integer localServerPort) {
        RestAssured.port = localServerPort;
        RestAssured.baseURI = "http://localhost";
        RestAssured.config =
                RestAssured.config()
                        .objectMapperConfig(
                                new ObjectMapperConfig()
                                        .jackson2ObjectMapperFactory((type, s) -> jackson));
        RestAssured.filters(
                new RequestLoggingFilter(),
                new ResponseLoggingFilter());
    }

    static Supplier<StartAuthenticationRequest> startAuthenticationRequestSupplier =
            () -> StartAuthenticationRequest.builder()
                    .username(USERNAME).password(PASSWORD).build();

    @Test
    void givenStartAuthenticationRequest_thenReturnOk() {
        AuthSpecifications.givenStartAuthenticationPostRequest(startAuthenticationRequestSupplier.get())
                .spec(AuthSpecifications.expectStartAuthenticationResponseOk());
    }

    @Test
    void givenCompleteAuthenticationRequest_thenReturnOk() {
        when(challengeResponseService.calculateChallenge(any())).thenReturn(Mono.just(new Pair<>("", "")));
        when(challengeResponseService.verifyResponse(any(), any(), any())).thenReturn(Mono.just(true));
        String sessionId = AuthSpecifications
                .givenStartAuthenticationPostRequest(startAuthenticationRequestSupplier.get())
                .extract()
                .jsonPath().get("session_id");
        CompleteAuthenticationRequest request =
                CompleteAuthenticationRequest.builder().challengeResponse("0000").build();
        AuthSpecifications.givenCompleteAuthenticationPatchRequest(sessionId, request)
                .spec(AuthSpecifications.expectCompleteAuthenticationResponseOk());
    }

    @Test
    void givenCompleteAuthenticationRequest_afterThreeInvalidRequests_thenReturnFailedSession() {
        when(challengeResponseService.calculateChallenge(any())).thenReturn(Mono.just(new Pair<>("", "")));
        when(challengeResponseService.verifyResponse(any(), any(), any())).thenAnswer(
                invocation -> Mono.just(invocation.getArgument(1).equals("1234")));
        String sessionId = AuthSpecifications
                .givenStartAuthenticationPostRequest(startAuthenticationRequestSupplier.get())
                .extract()
                .jsonPath().get("session_id");
        CompleteAuthenticationRequest request =
                CompleteAuthenticationRequest.builder().challengeResponse("0000").build();
        AuthSpecifications
                .givenCompleteAuthenticationPatchRequest(sessionId, request)
                .statusCode(HttpStatus.BAD_REQUEST.value());
        AuthSpecifications
                .givenCompleteAuthenticationPatchRequest(sessionId, request)
                .statusCode(HttpStatus.BAD_REQUEST.value());
        AuthSpecifications
                .givenCompleteAuthenticationPatchRequest(sessionId, request)
                .spec(new ResponseSpecBuilder()
                        .expectStatusCode(HttpStatus.OK.value())
                        .expectBody("status", is(SessionStatus.FAILED.name()))
                        .expectBody("token", is(emptyOrNullString()))
                        .build());
    }

    @Test
    void givenCompleteAuthenticationRequest_afterSessionCompleted_thenReturnBadRequest() {
        when(challengeResponseService.calculateChallenge(any())).thenReturn(Mono.just(new Pair<>("", "")));
        when(challengeResponseService.verifyResponse(any(), any(), any())).thenReturn(Mono.just(true));
        String sessionId = AuthSpecifications
                .givenStartAuthenticationPostRequest(startAuthenticationRequestSupplier.get())
                .extract()
                .jsonPath().get("session_id");
        CompleteAuthenticationRequest request =
                CompleteAuthenticationRequest.builder().challengeResponse("0000").build();
        AuthSpecifications.givenCompleteAuthenticationPatchRequest(sessionId, request)
                .spec(AuthSpecifications.expectCompleteAuthenticationResponseOk());
        AuthSpecifications
                .givenCompleteAuthenticationPatchRequest(sessionId, request)
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void contextLoads() {
        final String challengeCode = "0000";
        ChallengeResponseService service =
                new ChallengeResponseService(new ChallengeCodeService(List.of(challengeCode)));
        Pair<String, String> pair = Objects.requireNonNull(
                service.calculateChallenge("username1234").block());
        String challenge = pair.getA();
        assertEquals(challengeCode, challenge);
        String response = pair.getB();
        Boolean verified = service.verifyResponse(
                challengeCode, response, "username1234").block();
        System.out.println(verified);
    }
}
