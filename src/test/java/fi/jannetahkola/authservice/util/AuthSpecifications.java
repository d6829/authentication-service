package fi.jannetahkola.authservice.util;

import fi.jannetahkola.authservice.session.SessionStatus;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.ResponseSpecification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.emptyOrNullString;

public class AuthSpecifications {
    public static ValidatableResponse givenStartAuthenticationPostRequest(Object body) {
        return given()
                .basePath("/auth")
                .body(body)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .post()
                .then();
    }

    public static ValidatableResponse givenCompleteAuthenticationPatchRequest(String sessionId, Object body) {
        return given()
                .basePath("/auth")
                .body(body)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .patch(sessionId)
                .then();
    }

    public static ResponseSpecification expectStartAuthenticationResponseOk() {
        return new ResponseSpecBuilder()
                .expectStatusCode(HttpStatus.CREATED.value())
                .expectBody("session_id", is(not(emptyOrNullString())))
                .expectBody("challenge", is(not(emptyOrNullString())))
                .build();
    }

    public static ResponseSpecification expectCompleteAuthenticationResponseOk() {
        return new ResponseSpecBuilder()
                .expectStatusCode(HttpStatus.OK.value())
                .expectBody("status", is(SessionStatus.COMPLETED.name()))
                .expectBody("token", is(not(emptyOrNullString())))
                .build();
    }
}
