# Pull image
FROM eclipse-temurin:17

# Set work directory
WORKDIR /

# Copy the application JAR
COPY ["target/app.jar", "app.jar"]

# Not required but nice to have for informational purposes.
# You can see this with 'docker ps' after the container is up.
EXPOSE 8080

# Start the application
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=local", "/app.jar"]