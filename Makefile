redeploy:
	docker-compose down --remove-orphans
	./mvnw clean package -DskipTests
	docker-compose up -d --build --force-recreate
	make logs

redeploy-local:
	docker-compose down --remove-orphans
	./mvnw clean package -DskipTests -Dspring.profiles.active=local
	docker-compose up -d --build --force-recreate
	make logs

reload:
	docker-compose down --remove-orphans
	docker-compose up -d --build --force-recreate
	docker-compose logs -f

logs:
	docker-compose logs -f

createdb:
	cd files/database; ./init.sh