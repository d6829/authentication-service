#!/bin/bash
echo "(Re)creating database with seed data..."
docker exec -i auth-db /usr/bin/mysql < schema.sql
docker exec -i auth-db /usr/bin/mysql < seed.sql
echo "Database & seed data created."