DROP DATABASE IF EXISTS `auth`;
CREATE DATABASE IF NOT EXISTS `auth`;
USE `auth`;

SET foreign_key_checks = 0;

CREATE TABLE `user` (
    `id` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
);

SET foreign_key_checks = 1;